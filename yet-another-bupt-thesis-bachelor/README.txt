====== 北京邮电大学本科学位论文模板 ======

作者：Caspar Zhang <casparant@gmail.com>

系统需求
========
理论上来说Linux/Windows下均可运行，只需要
安装XeLaTeX编译器+常见宏包+xeCJK宏包。注意
xeCJK有高低版本之分，低版本可以支持0.995以
前的XeTeX/XeLaTeX。目前只在Linux下测试通
过，测试环境为:Ubuntu 10.04+源中的TeX包(应
该对应于texlive2009版本)

如何使用
========
在安装好所需的TeX相关软件和宏包后，只需修改
下列文件即可：

main.cfg: 包含了论文中需要填写的项目，比如
          论文名称、你的姓名等等。论文的致
          谢部分也放在了这里。

abstract.cfg: 包含了论文的中英文摘要。

main.tex: 论文的主体部分

bib.ref: 论文的参考文献库

appendix.tex: 论文的附录部分

理论上来说，修改了上述文件，让其符合你的需求
即可。

